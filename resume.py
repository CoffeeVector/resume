#!venv/bin/python3
import os
import jinja2

import json
from jsonschema import validate
from datetime import datetime

def load_resume_json():
    with open('schema.json') as schema_file:
        with open('cv.json') as resume_file:
            schema_json = json.loads(schema_file.read())
            resume_json = json.loads(resume_file.read())
            validate(resume_json, schema_json)
            return resume_json

def link_no_protocol(link):
    """ Displays link without the protocol

    """
    display_link = link.replace("https://", "")
    return f'\\textcolor{{blue}}{{\\href{{{link}}}{{{display_link}}}}}'
def link_with_protocol(link):
    """
    Displays link with the protocol
    """
    return f'\\textcolor{{blue}}{{\\href{{{link}}}{{{link}}}}}'

def date_format(date_string):
    date_object = datetime.strptime(date_string, "%Y-%m-%d")
    return date_object.strftime("%b %Y")

def escape(input_string):
    special_chars = "&%$#_{}~^\\"

def tailor_resume(resume_json, job_description_filename):
    import spacy

    with open(job_description_filename, "r") as f:
        job_description_str = f.read()

    nlp = spacy.load("en_core_web_md")  # make sure to use larger package!
    job_description = nlp(job_description_str)

    def job_description_similarity(experience_description):
        experience_description = nlp(experience_description)
        return job_description.similarity(experience_description)

    # sort experiences by most relevant
    resume_json["projects"].sort(key=lambda project: job_description_similarity("\n".join(project["highlights"])), reverse=True)
    resume_json["work"].sort(key=lambda work: job_description_similarity("\n".join(work["highlights"])), reverse=True)

    # sort bullets by most relevant
    for project in resume_json["projects"]:
        project["highlights"].sort(key=lambda highlight: job_description_similarity(highlight), reverse=True)
    for experience in resume_json["work"]:
        experience["highlights"].sort(key=lambda highlight: job_description_similarity(highlight), reverse=True)
    return resume_json

def main(build_directory='build', filename='Resume', git_hash=None, job_description_filename=None):
    resume_json = load_resume_json()

    if job_description_filename:
        resume_json = tailor_resume(resume_json, job_description_filename)
    else:
        resume_json["projects"].sort(key=lambda project: project["startDate"], reverse=True)
        resume_json["work"].sort(key=lambda project: project["startDate"], reverse=True)

    latex_jinja_env = jinja2.Environment(
        block_start_string = '\BLOCK{',
        block_end_string = '}',
        variable_start_string = '\VAR{',
        variable_end_string = '}',
        comment_start_string = '\#{',
        comment_end_string = '}',
        line_statement_prefix = '%%',
        line_comment_prefix = '%#',
        trim_blocks = True,
        autoescape = False,
        loader = jinja2.PackageLoader('resume', 'templates')
    )

    latex_jinja_env.globals.update(link_no_protocol=link_no_protocol)
    latex_jinja_env.globals.update(link_with_protocol=link_with_protocol)
    latex_jinja_env.globals.update(date_format=date_format)

    resume_template = latex_jinja_env.get_template('Resume.tex.jinja')

    print(resume_template.render(
        git_hash=f"{git_hash}-{job_description_filename}" if job_description_filename else git_hash,
        **resume_json
    ), file=open(build_directory + "/" + filename, 'w+'))
