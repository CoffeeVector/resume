
build_directory = build
cv_filename = CV
resume_filename = Resume

.PRECIOUS: $(build_directory)/%.tex

all: $(build_directory)/$(cv_filename).pdf $(build_directory)/$(cv_filename).jpg

$(build_directory)/%.pdf: $(build_directory)/%.tex $(build_directory)
	@pdflatex -output-directory=$(build_directory) $< > /dev/null
	@test -f $@ || (echo "pdf did not generate." && exit 1)

$(build_directory)/%.jpg: $(build_directory)/%.pdf
	convert -density 300 $(build_directory)/$*.pdf +append $(build_directory)/$*.jpg
	@test -f $@ || (echo "jpg did not generate." && exit 1)

clean:
	rm -rf $(build_directory)

$(build_directory)/%.tex: resume.py .venv cv.json $(build_directory) templates
ifdef job_description_filename
	.venv/bin/python -c "import resume; resume.main('$(build_directory)', '$*.tex', git_hash='$$(git rev-parse --short HEAD)', tailor=True, job_description_filename='$(job_description_filename)')"> resume.log
else
	.venv/bin/python -c "import resume; resume.main('$(build_directory)', '$*.tex', git_hash='$$(git rev-parse --short HEAD)')"> resume.log
endif

.venv: requirements.txt
	python -m venv .venv
	.venv/bin/pip install --upgrade pip
	.venv/bin/pip install -r requirements.txt

$(build_directory):
	mkdir $(build_directory)
